# Week 8 Meeting Minutes

## Tutorial
    
There is no tutorial today.

## Team Meetup
    
    Time: 26 Apr. 2:00 ~ 4:00PM
    Participants: Aidapp team

### Activities:
1. Discussing Poster content and design
    - More deliverables and demos
    - Focusing on output
    - Using lighter colours

## Meeting with client
    
    Time: 26 Apr. 6:00 ~ 7:30PM
    Participants: Aidapp team, Boom (Company's Developer)

### Activities:

1. Reported changes since last week
2. Discussed the reseasons for delay of 'opp fields udpdation' issues:
   - Last week's task assigning wasn't confirmed carefully, so we became more careful confirming this week's tasks
