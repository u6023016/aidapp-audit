# Week 9 Meeting Minutes

## Tutorial
    
    Time: 3 May 10:00AM ~ 12:00PM
    Participants: Aidapp team

### Activities:
1. Talked with tutor about recent progress
2. Let tutor inspect our poster draft and give us some advice
    - we modified some wording
    - and deleted unecessary content
3. Confirmed requirements of WPP with tutor

## Team Meetup
    
    Time: 3 May 2:00PM ~ 4:00PM
    Participants: Aidapp team

### Activities:
1. Finalised poster
2. Discussed newly found bugs
    - user journey of 'blocking', 'unblocking'
    - searching opportunity sometimes mismatches

## Meeting with client
    
    Time: 3 May 6:00PM ~ 7:30PM
    Participants: Aidapp team, Boom (Company's Developer)

### Activities:

1. Reported changes since last week
2. Discussed plan of ending the project with client
3. Assigned last tasks