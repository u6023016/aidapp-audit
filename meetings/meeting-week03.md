## Meeting with client
    
    Time: 8 Mar. 6:00 ~ 7:30PM
    Participants: Boom (Company's Developer), Xiangyou, Ruibiao, Zhenyu, Yuting, Qingli

### Issues discussed:

1. Tasks on issue board are too generic to track and implement:
    - Decission: spliting to smaller, traceable sub-tasks
    - Result: Done
1. Client needs time estimation of tasks to set company's plan
    - Decission: estimating more exact time for each sub-task of project
    - Result: Done