## Meeting with client
    
    Time: 1 Mar. 5:30 ~ 6:30PM
    Participants: Boom (Company's Developer), Xiangyou, Yitao, Ruibiao, Zhenyu, Yuting, Qingli

### Events:

1.  Discussed the serval tasks that we should do in this semester.
2.  Discussed the importance of each tasks and arranged the finishing order of each task as some tasks are urgently to be finished from client's requirement.
2.  Discussed and designed the basic version of the new data storing logic for chat.
3.  Discussed and designed the data model for opportunities. 
4.  Discussed and decided the new role for each team member in this semester because in some period of this semester we will begin serval tasks at the same time so it's important to distribute the tasks and roles.
5.  Discussed the working time and workload with the client. Discussed the regular meeting time and method with cilent for efficient communication.
6.  Discussed and selected a proper time that the whole team can sit together and code cooperatively because we think by this way we efficent coding and making the whole process more smoothly.



