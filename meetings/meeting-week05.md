## Tutorial
    
    Time: 22 Mar. 10:00AM ~ 12:00PM
    Participants: Tutor, Shadow team, AidApp team

### Events:

1. Shadow team demo: members will push ideas on trello under each task to make decisions quickly
    - A good practice
2. Tutor: think about make something that can demo in the next audit
    - Decision: accelerating frontend and backend integration on new code

## Team Meetup
    
    Time: 22 Mar. 2:00 ~ 4:00PM
    Participants: AidApp team

### Activities:
1. Coding
2. Block out the structure of the presentation next week


## Meeting with client
    
    Time: 22 Mar. 6:00 ~ 7:30PM
    Participants: Boom (Company's Developer), Xiangyou, Ruibiao, Yitao, Zhenyu, Yuting, Qingli

### Activities:
1. Added new improvement tasks on emails/phones management
2. Fixed bugs
3. Reported changes since last week