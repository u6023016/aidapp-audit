# Week 7 Meeting Minutes

## Tutorial
    
    Time: 19 Apr. 10:00AM ~ 12:00PM
    Participants: Tutor, Shadow team, Aidapp team

### Events:

1. Summarised feedback received from the Audit 02
2. Introduced the testing procedure we proposed in mid-break to tutor and shadow team

## Team Meetup
    
    Time: 19 Apr. 2:00 ~ 4:00PM
    Participants: Aidapp team

### Activities:
1. Testing with shadow team
    - Identified some problem of our APP
    - Shadow provided many good suggestions

## Meeting with client
    
    Time: 19 Apr. 6:00 ~ 7:30PM
    Participants: Aidapp team, Boom (Company's Developer)

### Activities:

1. Reported changes since last week
2. Reported issues spotted from tests
3. Discussed solutions and added new cards