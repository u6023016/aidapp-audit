## Tutorial
    
    Time: 15 Mar. 10:00AM ~ 12:00PM
    Participants: Tutor, Shadow team, AidApp team

### Issues discussed:

1. Meeting minutes lack details
    - Decision: adding more details like participants and time, formatting
    - Result: Done
2. Communication with client is not streamlining:
    - Decision: 
        - migrating tasks on Gitlab issues to Trello
        - integrating Trello with Slack
        - once making managerial decisions, talking with client first
    - Result: Done

## Meeting with client
    
    Time: 15 Mar. 6:00 ~ 7:30PM
    Participants: Boom (Company's Developer), Xiangyou, Ruibiao, Yitao, Zhenyu, Yuting, Qingli

### Activities:
1. Migrated task cards
2. Discussed convention of using Trello
3. Reported changes since last week
