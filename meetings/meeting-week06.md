# Week 6 Meeting Minutes
    Tutorial
        Time: 29 Mar. 10:00AM ~ 12:00PM
        Participants: Tutor, Shadow team, Aidapp team
    Team Meetup
        Time: 29 Mar. 2:00 ~ 4:00PM
        Participants: Aidapp team
    Meeting with client
        Time: 29 Mar. 6:00 ~ 7:30PM
        Participants: Aidapp team, Boom (Company's Developer)
        
1.  Link your subtasks (shown in Trello) to your higher level strategic plan. Show the subtasks in higher level plan - with estimated timeframe for each.
  - We've marked each card with labels which correspond to our higher level plan, such as "opportunity", "organisation"...
2.  Put the two-week semester break on the high level plan and show on it any activities that will take place across that time.
  - Arranged
3.  Make sure that all tasks in Trello are tagged to one of the high level objectives on the plan
  - Same as 1
4.  Program regular user testing on to the Trello board in consultation with client.
  - Result: Testing cards were put in trello
5.  Develop a set of objectives you have for testing (what are you looking for?) – these should be according to category (user experience, functionality, integrity of underlying engineering, identification and capture of bugs/issues, etc). Liaise with client on testing and feedback regime.
  - Smae as 4
6.  Convene to discuss prioritisation of bugs/issues. Check with lead developer, then program in to-do on Trello according to priority.
  - Result: discussed priority levels: critical, high, medium, low..
7.  Reconsider how you will document – suggested that documentation occurs on an ongoing basis, not in a block at the end. Program documentation weekly in Trello.
  - Result: documents are added with code in progress in comments
8.  Increase productivity and evidence of development of the application. 
  - Result: more active slack, trello involvement 
