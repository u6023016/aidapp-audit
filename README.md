# Audit Landing Page of Aidapp
## Introduction
### About this page
This is the landing page for the Techlauncher AidApp project. 
You can find links to useful resources that are necessary for reviewing the project, 
such as links to our repositories, audit files, artefacts, milestones, progress.

### About our Project
AidApp is a platform that connects talents, opportunities and solutions based on locations. 
Users of AidApp can establish their social network, seek opportunities and form teams, etc. 
The platform only focuses on specific sectors related to humanitarianism and aid. In other words, it is not an App for general career development. 
It is for rapid formation of local aid teams or opportunities of short-term work in a certain area. 

## Tools
- <a href="https://bitbucket.org/aidappteam/profile/repositories" target="_blank">Bitbucket</a> as code repository  
  - AidApp (including Opportunity and Chat modules) in [Frontend-Ionic2](https://bitbucket.org/aidappteam/frontend-ionic2)
  - AidApp backend in [Backend-Rails](https://bitbucket.org/aidappteam/backend-rails)
  - Chat backend in [Backend-NodeJS](https://bitbucket.org/aidappteam/backend-nodejs)
  - Organisation: has not created
- <a href="https://trello.com/b/9BJI2S1g/1-bugs-issues" target="_blank">Trello</a> for task management  
  -  The cards related to our team are all on 'Bugs & Issues' board
  -  Our cards are labeled in titles as shown in the milestone image
  -  Team members in charge of a card are assigned to its member
- <a href="https://slack.com/" target="_blank">Slack</a> for communication with client

## Audit Presentations
Please visit the <a href="https://gitlab.cecs.anu.edu.au/u6023016/aidapp-audit/tree/master/audit-presentations">/audit-presentations</a> directory for details of our presentaions.

## Milestones
<!--Please view <a href="https://gitlab.cecs.anu.edu.au/u6023016/aidapp-audit/milestones" target="_blank">here</a> for details of our milestones.-->
Gantt chart of our project. Click to image to view the PDF file.
<a href="AidApp-Techlauncher-gantt.pdf"><img src="gantt-chart-final.png" width="90%"></a>

## Meetings and Decisions
Decisions usually are made in meetings by group discussion, and important 
managerial decisions will be communicated with client. 
[Meeting minutes](/meetings) record problems arised from the shadow, tutor, 
client or the team itself, and corresponding decisons, actions to address these problems.  
#### __Meeting Schedule:__
- Tutorials: Thursday 10:00 ~ 12:00
- Team member meetups: every Thursday 14:00 ~ 16:00
- Client meetings: every Thursday 18:00 ~ 19:00

## Team Members
- Xiangyou Zhang: <a href="mailto:u5992627@anu.edu.au">u5992627@anu.edu.au</a>
- Yitao Zhuo: <a href="mailto:u6023016@anu.edu.au">u6023016@anu.edu.au</a>
- Zhenyu Zhao: <a href="mailto:u5983844@anu.edu.au">u5983844@anu.edu.au</a>
- Ruibiao Zhu: <a href="mailto:u5861758@anu.edu.au">u5861758@anu.edu.au</a>
- Yuting Lu: <a href="mailto:u6020162@anu.edu.au">u6020162@anu.edu.au</a>
- Qingli Guo: <a href="mailto:u6011074@anu.edu.au">u6011074@anu.edu.au</a>

## Notes
- **The Bitbucket repositories are private and they are managed by our client. One may need permission to access.** Plese contact one of the team members for permission.
- If you are interested in our App, the AidApp, **there is a testing version available in TestFlight** (for iOS). You can email us for a redeem code.
- The landing page of Aidapp for semester 2 2017 is at https://gitlab.com/Aidapp/computing-project.  
However, it's private becasue it contains some data that our client wants to protect. 
If you need access to it, please contact one of the team members.