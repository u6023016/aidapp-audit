# Aidapp Rails Backend APIs


## Profile

|Function Name|Example|Description|Routes|Author|
| ------------ | --- | ------------------------------- |----- |--- |
|getprofile_v2|Inputs: {"user_id": int //user_id, "status": string //status}  <br>Outputs: success:{ "profile":[],"shared_conn_name":{:} }, fail: {"status":error};|Get the profile and the user which it connects|post '/api/v2/getprofile'|Yuting Lu|
|register|Input:{ "user_id": int //user_id,"email": string //"email","phone": string //"phone","name": string//"name"}<br>Outputs: success:{"status": 200 },fail:{"status": 400}|Register information and create connection with admin|post '/api/register'|Harris|
|insertprofile|Inputs:{"user_id": int //user_id,"profile":"current_location": string//"location","country": string //"country","org": string//"organisation","position": string//"your position","skill": string//"your skills","sector": string//"sector"}<br>Outputs: success: {profile: {}, 200},fail:{"status": 400}|Insert profile|post '/api/insertprofile'|Harris|
|get_contact_details|Inputs: {"user_id": int //user_id, "other_id": int //other_id}<br>Outputs: {"connStatus": [connStatus_a2b , connStatus_b2a], profile:{}, sharedConnName: {}}|returns a user's profile along with the connection status and shared connections between you and the user.|post '/api/v2/getContactDetails'|Xiangyou Zhang|

## Opportunity

|Function Name|Example|Description|Routes|Author|
| ------------ | --- | ------------------------------- |----- |--- |
|create_opp|Inputs: {"user_id": int //user_id, "opp_detail": {"key": string //"value"}}<br>Outputs: {status: 200, opp_id: int} // if succeeded|Creates an opportunity and inserts opp_detail|post 'api/postOpp'|Zhenyu Zhao|
|delete_opp| Inputs: {"opp_id": int //opp_id}<br>Outputs: success:{ "status": 200}, fail:{"status": 400};|Delete opportunity|delete 'api/deleteOpp'|Yuting Lu|
|search_opps|Inputs: {criteria: {key: string //value}, req_fields: [str]} // req_fields: take names of fields that you wish to use in list display<br>Outputs: {status: 200, result: [{opp_id: int, matches: int, created_at: date, (any required fields)}]};|Search opportunities by criteria|post 'api/searchOpps'|Xiangyou Zhang|
|get_opp_details |Inputs: {"opp_id": int //opp_id}<br>Outputs: {"opp_detail":[{key: "key", value: value in json, id: int, opportunity_id: opp_id}]}|Get the opportunity and the user which it connects|get 'api/getOppDetails'|Zhenyu Zhao|
|get_all_opp_details|Outputs:{"opp_detail":{"key1":"value1","key2":"value2",...,"keyN":"valueN"}}|Get all opportunities|post 'api/getOppDetails2'|Yuting Lu|
|update_opp_details|Inputs: {"opp_id": int //opp_id,"opp_detail":{"key1": string //"value1","key2": string //"value2",...,"keyN": string //"valueN"}}<br>Outputs: success:{ "status": 200}, fail:{"status": 400};|Update opportunity details|post 'api/updateOppDetails'|Yuting Lu|

## Entity Info

|Function Name|Example|Description|Routes|Author|
| ------------ | --- | ------------------------------- |----- |--- |
|getentityinfo_v2|Inputs: {"user_id": int //user_id, "status": string //status}<br>Outputs: {"status": int,"connections_details": @connection,"entity": [@user_info, {key: 'sharedConnNum', value: int]}}|Gets entity info of all contacts of the given id. These method is going to be replaced by get_profile(could be other names)|post 'api/v2/getentityinfo' |Harris, modified by Xiangyou|
|getsharedconnname_v2|Inputs: {"user1_id": int //user1_id, "user2_id": int //user2_id}<br>Outputs: user ids and names of shared contacts {user_id: "name",...}|Gets names of shared contacts. Data is retrieved from Connection and Profile|post 'api/v2/getsharedconnname'|Xiangyou Zhang|
|searchcontact_v2|Inputs: {"user1_id": int //user1_id, "user2_id": int //user2_id}<br>Outputs: user ids and names of shared contacts {user_id: "name",...}|Searching contacts according to the given criteria and degree. Searching is based on the connection network of the searching user|post 'api/v2/searchcontact'|Harris, modified by Xiangyou|
|flag_user |Inputs: {"user_id": int //user_id}<br>Outputs: success:{"status":200}|Adds a flag to a user by increase flag_count by 1.|post 'api/flagUser'|Harris|
|get_user_flag|Inputs: {"user_id": int //user_id}<br>Outputs: Flag count|Get flag users.|get  'api/getUserFlagCount/:user_id'|Harris|
|clear_user_flag |Inputs: {"user_id": int //user_id}|Clears the flag of a user by setting flag_count to 0.|patch 'api clearUserFlagCount/:user_id'|Harris|
|get_ban_users_account_status|Inputs: nothing<br>Outputs: Tuples of User table.|Gets user account info of all banned users.|get 'api/getBanUsers' |Harris|
|ban_user|Inputs: {user_id: int}<br>Outputs: success:{"status":200}|Ban a user.|post 'api/banUser' |Harris|

## Connection

|Function Name|Example|Description|Routes|Author|
| ------------ | --- | ------------------------------- |----- |--- |
|checkpendingconn|Inputs: {"user_id": int //user_id}<br>Outputs: Array of connection tuples of where status is 1(pending).|Checks the pending connections of the user and returning these if any.|get '/api/checkpendingconn'|Harris|
|findconnbypk|Inputs: {"my_entity_pk": int //"PK"+user_id1, "connection_pk": int //"PK"+user_id2}<br>Outputs: Arrays|Finds connections with given PKs.|post '/api/findconnbypk'|Harris|
|createconn_v2|Inputs: {"user1_id": int //user1_id, "user2_id": int //user2_id}<br>Outputs: success:{ "status": the created connections of user1 and user2 200}, fail:{"status": 400};|Creates connections and creates corresponding items in Entity_info|post '/api/v2/createconn'|Xiangyou Zhang|
|block_user_v2|Inputs: {"user_id": int // user_id, "block_user_id": int // block_user_id<br>Output: success: {"status": 200}, fail: {"status": 401, 'blocking failed for errors: '+err_messages}  |Block a user so that the user will no longer see your profile. Any user can be blocked, no matter the connection is established or not.|post '/api/v2/blockUser'|Xiangyou Zhang|
|unblock_user_v2|Inputs: {"user_id": int //user_id,"block_user_id": int // block_user_id<br>Output: success: {"status": 200} fail: {"status": 401, 'unblocking failed for errors: '+err_messages}|Unblock a user|post '/api/v2/unblockUser'|Xiangyou Zhang|
|delete_connections|Inputs: {"user_id": int // user_id, "other_id": int // other_id}<br> Output: success: {"status": 200}, fail: {"status":400}|Accepts the connections between two users. Only the connections with PENDING_ACCEPT on user side and REQUEST_SENT on other side will be accepted.|delete '/api/v2/deleteConnections'|Xiangyou Zhang|
|reject_connection_request|Inputs: {"user_id": int // user_id, "other_id": int // other_id}<br>Output: success: {"status": 200}, fail: {"status":400}|Rejects a connection request. Only the connections with PENDING_ACCEPT on user side and REQUEST_SENT on other side will be deleted.|delete '/api/v2/rejectConnRequest'|Xiangyou Zhang|
|accept_connection_request|Inputs: {"user_id": int // user_id, "other_id": int // other_id}<br>Output: success:{"status: 200"}, fail: {"status":400}|Cancels a connection request. Only the connections with REQUEST_SENT on user side and PENDING_ACCEPT on other side will be deleted.|post '/api/v2/acceptConnRequest'|Xiangyou Zhang|
|cancel_connection_request|Inputs: {"user_id": int // user_id, "other_id": int // other_id}<br>Output: success:{"status: 200"}, fail: {"status":400}|Cancels a connection request. Only the connections with REQUEST_SENT on user side and PENDING_ACCEPT on other side will be deleted|delete '/api/v2/cancelConnRequest'|Xiangyou Zhang|
|global_search_v2|Inputs: {"user_id": int // id of the user who searches "criteria": {"name": "","current_location": "","country": [], "org": "", "position": "", "skills": "", "sector": []}}<br>Outputs: success: [{"profile_item1": "value1", "profile_item2": "value2", ..., "user_id": <int>, "matchedFieldNum": <int>, "sharedContacts": [...]], fail:{"status": 400}|Search all users with given criteria matched profiles|post '/api/v2/globalSearch'|Xiangyou Zhang|
|get_all_contacts_test|Outputs: {all contacts}|Get all contacts|get '/api/v2/getAllContacts_test'|Xiangyou Zhang|

## User

|Function Name|Example|Description|Routes|Author|
| ------------ | --- | ------------------------------- |----- |--- |
|session#create|Input: {user: {password: <str> - the verification code}} <br>Output:  {"status": 200, "authentication_token": <token>, "user_id": <int>, "user_email": <str>, "login_status": <int>,"user_details": [<entity_info>]}|Creates a session when a user logins in and returns a token to the user.|post '/api/login'|Harris|
|session#destroy|Input: {user_id: <int>} <br>Output: {}|Deletes a user from the system, including everything the user created, and expires the token.|delete '/api/logout'|Harris|
|invitefromapp|Input: {id: <int> //inviter's id, user:{email: <str>, phone: <str>}} <br>Output: {status: <int>, him: <int> //invitee's id}|Invites a person|post '/api/invitefromapp'|Harris|
|forgot_referral_code|Input: {email: <str>} <br>Output: {status: <int>}|Recovers account by sending a new verification code to email.|post '/api/forgot_referral_code'|Harris|
|invitefrombrowser|Input: {email: <str>, phone: <str>, free_text: <str> //description } <br>Output: redirection to a page|Invites from browser when user accesses to Aidapp.io.|post '/api/invitefrombrowser|Harris|
|migraterc|Input: {id: <int>} <br>Output: {status: <int>}|?|post '/api/migraterc'|Harris|
|check_version|Inputs: {"user_id": int //user_id, "version": int //version_id}<br>Outputs: latest version : {true, my_version}, not latest version: {false, my_version, please upgrade to the latest version "current_version"}|Checks current app version|post '/api/check_version'|Harris|
|activate_new_email|Input: {id: <int>} <br>Output: redirection to a page|Verifies an email added by a user.|get '/api/activatenewemail/:id'|Harris|
|checkstatus|Input: {id: <int>} <br>Output: <user>|Returns data of the user in database.|get '/api/checkstatus/:id'|Harris|